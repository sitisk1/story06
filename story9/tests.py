from django.test import TestCase
from django.test import TestCase
from django.urls import resolve
from .views import index, user_login, user_logout, register
from .forms import MakeUser
from django.contrib.auth.models import User
from django.contrib import auth
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class TestUrlAndViews(TestCase):
	def test_url_story9_ada(self):
		response = self.client.get('/story9/')
		self.assertEqual(response.status_code, 200)
	def test_template_benar_ga(self):
		response = self.client.get('/story9/')
		self.assertTemplateUsed(response,'index.html')
	def test_pake_fungsi_index(self):
		fungsi = resolve('/story9/') 
		self.assertEqual(fungsi.func, index)
	def test_url_login_ada(self):
		response = self.client.get('/story9/login/')
		self.assertEqual(response.status_code, 200)
	def test_template_login_benar_ga(self):
		response = self.client.get('/story9/login/')
		self.assertTemplateUsed(response,'login.html')
	def test_pake_fungsi_user_login(self):
		fungsi = resolve('/story9/login/') 
		self.assertEqual(fungsi.func, user_login)
	def test_url_register_ada(self):
		response = self.client.get('/story9/register/')
		self.assertEqual(response.status_code, 200)
	def test_template_register_benar_ga(self):
		response = self.client.get('/story9/register/')
		self.assertTemplateUsed(response,'register.html')
	def test_pake_fungsi_register(self):
		fungsi = resolve('/story9/register/') 
		self.assertEqual(fungsi.func, register)
	def test_url_logout_ada(self):
		response = self.client.get('/story9/logout/')
		self.assertEqual(response.status_code, 302)
	def test_pake_fungsi_user_logout(self):
		fungsi = resolve('/story9/logout/') 
		self.assertEqual(fungsi.func, user_logout)
class TestRegisterLogin(TestCase): 
	@classmethod
	def setUpTestData(cls):
		user = User.objects.create_user(username='sitisk', email='sitisk@gmail.com', password='sitisk')
		user.save()

	def test_apakah_login(self):
		response = self.client.post('/story9/login/', data={'username':'sitisk', 'password':'sitisk'})
		exist = auth.get_user(self.client)
		self.assertTrue(exist.is_authenticated)

		html_response = response.content.decode('utf8')
		self.assertIn(html_response, 'Halo: sitisk!')

	def test_apakah_register(self):
		regist = self.client.post('/story9/register/', data={'username':'cuy','email':'cuy@gmail.com','password':'cuy', 'konfirmasi_password':'cuy'})
		response = self.client.get('/story9/register/')
		self.assertEqual(response.status_code, 200)

class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()
