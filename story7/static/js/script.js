$(document).ready(function(){
    var click = true;
    $("#button-theme").click(function(){
        if(click){
            $('body').css('background-color', 'black');
            $('label').css('color', 'white');
            $('#sub-title').css('color', 'white');
            $('td').css('color', 'white'); 
            $('th').css('color', 'white');
            $('.isian').css({
            	'background-color':'black',
            	'color':"white",
            });

            click = false;
        } else {
        	$('body').css('background-color', 'white');
            $('label').css('color', 'black');
            $('#sub-title').css('color', '#50423C');
            $('td').css('color', 'black'); 
            $('th').css('color', 'black'); 
            $('.isian').css({
            	'background-color':'whitesmoke',
            	'color':"black",
            });
            click = true;
        };
    })
    $(function(){
    	$("#accordion").accordion({
    		collapsible:true,
    		active: false,
    		heightStyle: "content",
    	});
  	});
});
