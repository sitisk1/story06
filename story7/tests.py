from django.test import TestCase
from django.test import TestCase
from django.urls import resolve
from .views import addstatus
from .models import StatusModel
from .forms import StatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class TestUrlAndViews(TestCase):
	def test_url_status_ada(self):
		response = self.client.get('/storyke7/')
		self.assertEqual(response.status_code, 200)
	def test_template_benar_ga(self):
		response = self.client.get('/storyke7/')
		self.assertTemplateUsed(response,'story7.html')
	def test_pake_fungsi_addstatus(self):
		fungsi = resolve('/storyke7/') 
		self.assertEqual(fungsi.func, addstatus)
class TestModel(TestCase): 
	@classmethod
	def setUpTestData(cls):
		StatusModel.objects.create(status='Today is a big day')
	def test_create_object(self):
		count = StatusModel.objects.all().count()
		self.assertEqual(count,1)
	def test_status_str(self):
		obj = StatusModel.objects.get(id=1)
		expect = obj.status
		self.assertEqual(expect, str(obj))

class TestForm(TestCase):
	def test_apakah_ke_post(self):
		status = 'Today is a big day?'
		response = self.client.post('/storyke7/', data={
			'status':status})
		self.assertEqual(response.status_code, 302)

		form = StatusForm(data={
			'status':status})
		self.assertTrue(form.is_valid())

		response_get = self.client.get('/storyke7/')
		html_response = response.content.decode('utf8')
		self.assertIn(html_response, status)

	def test_apakah_gak_ke_post(self):
		status = 'Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?Today is a big day?'
		response = self.client.post('/storyke7/', data={
			'status':status})
		self.assertEqual(response.status_code, 302)
		response_get = self.client.get('/storyke7/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(status, html_response)

	def test_status_field_placeholder(self):
		form = StatusForm()
		self.assertIn('placeholder="Masukkan kabar kamu hari ini :)', form.as_p())

class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_inputan(self):
		self.browser.get('http://siti-storyenam.herokuapp.com/storyke7')
		time.sleep(3)
		temp_status = self.browser.find_element_by_name('status')
		temp_submit = self.browser.find_element_by_name('simpan')
		temp_status.send_keys('Coba Coba')
		time.sleep(3)
		temp_submit.send_keys(Keys.RETURN)
		time.sleep(3)
		self.assertIn('Coba Coba', self.browser.page_source)

	def test_change_theme(self):
		self.browser.get('http://siti-storyenam.herokuapp.com/storyke7')
		time.sleep(3)
		temp_body = self.browser.find_element_by_tag_name('body')
		temp_label = self.browser.find_element_by_tag_name('label')
		temp_sub = self.browser.find_element_by_id('sub-title')
		temp_td = self.browser.find_element_by_tag_name('td')
		temp_th = self.browser.find_element_by_tag_name('th')
		temp_button = self.browser.find_element_by_name('theme-btn')
		temp_isian = self.browser.find_element_by_class_name('isian')
		temp_button.send_keys(Keys.RETURN)
		time.sleep(3)
		background_body = temp_body.value_of_css_property('background-color')
		background_label = temp_label.value_of_css_property('color')
		background_sub = temp_sub.value_of_css_property('color')
		background_td =temp_td.value_of_css_property('color')
		background_th =temp_th.value_of_css_property('color')
		color_isian =temp_isian.value_of_css_property('color')
		background_isian =temp_isian.value_of_css_property('background-color')
		self.assertEqual(background_body, 'rgba(0, 0, 0, 1)')
		self.assertEqual(background_label, 'rgba(255, 255, 255, 1)')
		self.assertEqual(background_sub, 'rgba(255, 255, 255, 1)')
		self.assertEqual(background_td, 'rgba(255, 255, 255, 1)')
		self.assertEqual(background_th, 'rgba(255, 255, 255, 1)')
		self.assertEqual(background_isian,'rgba(0, 0, 0, 1)')
		self.assertEqual(color_isian, 'rgba(255, 255, 255, 1)')
# # Create your tests here.
