from django import forms
from status.models import StatusModel

class StatusForm(forms.ModelForm):
	class Meta:
		model = StatusModel
		fields = ['status']
		widgets ={
		'status':forms.Textarea(attrs={'class':'form-control', 
										'placeholder': 'Masukkan kabar kamu hari ini :)'}),
		}