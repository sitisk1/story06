from django.shortcuts import render, redirect
from .models import StatusModel
from .forms import StatusForm
from django.utils import timezone
# Create your views here.
def add_status(request):
	if request.method =='POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			if (len(form.cleaned_data['status']) < 301): # maksimal input status 300 char
				post = form.save(commit=False)
				post.time = timezone.now() 
				post.save()
				return redirect('/')
			else:
				return redirect('/')
	else:
		form = StatusForm()
	objects = StatusModel.objects.all()
	isi = {'generate_form': form,
			'all_objects':objects}
	return render(request,'status.html',isi)