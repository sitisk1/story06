from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
	path('', views.list_books, name='books'),
	path('json/<str:keyword>/', views.process_json, name='process_json'),
] 