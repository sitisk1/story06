from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
# Create your views here.
def list_books(request):
	return render(request, 'story8.html')

def process_json(request, keyword):
	r = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + keyword)
	return JsonResponse(r.json())
	