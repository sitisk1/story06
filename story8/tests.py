from django.test import TestCase
from django.test import TestCase
from django.urls import resolve
from .views import process_json, list_books

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class TestUrlAndViews(TestCase):
	def test_url_books_ada(self):
		response = self.client.get('/books/')
		self.assertEqual(response.status_code, 200)
	def test_template_benar_ga(self):
		response = self.client.get('/books/')
		self.assertTemplateUsed(response,'story8.html')
	def test_pake_fungsi_list_books(self):
		fungsi = resolve('/books/') 
		self.assertEqual(fungsi.func, list_books)
	def test_url_json_ada(self):
		response = self.client.get('/books/json/percy/')
		self.assertEqual(response.status_code, 200)
	def test_pake_fungsi_process_json(self):
		fungsi = resolve('/books/json/percy/') 
		self.assertEqual(fungsi.func, process_json)
	def test_ada_tulisan_halo_gak(self):	
		response = self.client.get('/books/')
		html_response = response.content.decode('utf8')
		self.assertIn("Halo, Mau Baca Buku?", html_response)
class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()
	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()
	def test_poetry(self):
		self.browser.get('http://siti-storyenam.herokuapp.com/books')
		temp_button = self.browser.find_element_by_id('btn-search')
		temp_isian = self.browser.find_element_by_id('area-search')
		temp_isian.clear()
		temp_isian.send_keys('Poetry')
		time.sleep(3)
		temp_button.click()
		time.sleep(3)

		self.assertIn('Poetry', self.browser.page_source)
		self.assertIn('<td>', self.browser.page_source)