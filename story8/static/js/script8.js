$(document).ready(function(){
    $('#btn-search').click(function(){
            var keyword = $('#area-search').val();
            $.ajax({
                url:'/books/json/' + keyword,
                success: function(data){
                    $('#per-buku').empty();
                    var items = data.items;
                    for(var i =0; i<6; i++){
                        $('<tr>').append(
                            $('<td>').text((i+1)),
                            $('<td>').append('<img style="height:200px; width: 200px" src=' + items[i].volumeInfo.imageLinks.thumbnail + "'/>"),
                            $('<td>').text(items[i].volumeInfo.title),
                            $('<td>').text(items[i].volumeInfo.authors),
                            $('<td>').text(items[i].volumeInfo.publisher),
                            ).appendTo('#per-buku');
                    }
                }    
            });
    });
    $('#area-search').val("percy");
    $('#btn-search').click();
});
